# Barcelona

Text amb informació de Barcelona, bonica ciutat del Mediterrani.

## Llista ordenada de barris

1. Noubarris
2. Guinardo
3. Trinitat

## Llista desordenada de barris

- Trinitat Nova
- Guineuta
- Ciutat Vella

**Enllaç a una web** [enllaç a Barcelona](https://www.fcbarcelona.es/es/)

## BARCELONA

![Imatge de Barcelona](../imagenes/BarcelonaImg.png "Barcelona")
